# Color definitions
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[1;32m'
NC='\033[0m'

# Installition
echo -e "${GREEN}STARTING INSTALLITION...${NC}"
echo -e "${YELLOW}Preforming apt update...${NC}"
sudo apt update -y
echo -e "${YELLOW}Preforming apt upgrade...${NC}"
sudo apt upgrade -y
echo -e "${YELLOW}Installing build tools...${NC}"
sudo apt install -y git cmake
echo -e "${YELLOW}Installing sdks...${NC}"
sudo apt install -y wiringpi freenect libsfml-dev zlib1g-dev libglm-dev
echo -e "${YELLOW}Installing multicast DNS...${NC}"
sudo apt install -y avahi-daemon
echo "voxelmapper" > /etc/hostname
echo -e "${YELLOW}MDNS: voxelmapper.local${NC}"
echo -e "${GREEN}INSTALL FINISHED!!!${NC}"
