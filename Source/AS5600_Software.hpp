#pragma once

#include "soft_i2c.hpp"
#include <stdint.h>

class AS5600_Software
{
private:	
	enum RegisterMap : uint8_t
	{
		ZMCO		= 0x00,
		ZPOS		= 0x02,
		MPOS		= 0x03,
		MANG		= 0x05,
		CONF 		= 0x07,
		RAW_ANGLE	= 0x0C,
		ANGLE		= 0x0E,
		STATUS		= 0x0B,
		AGC			= 0x1B,
		MAGNITUDE	= 0x1C,
		BURN		= 0xFF
	};
	
	static constexpr float degScale = 360.f / (1<<12);
	static constexpr uint8_t deviceId = 0x36;
	i2c_t i2c; 
	
public:
	AS5600_Software(const int8_t& scl,const int8_t& sda)
	{
		i2c = i2c_init(scl,sda);
	}
	
	const float GetAngleDegrees()
	{
		i2c_start(i2c);
		if (i2c_send_byte(i2c, deviceId << 1 | I2C_WRITE) != I2C_ACK) {
			std::cerr << "Software AS5600 error! No device found!"<<std::endl;
			return -1;
		}
		i2c_send_byte(i2c, RegisterMap::RAW_ANGLE);
		
		i2c_start(i2c);
		i2c_send_byte(i2c, deviceId << 1 | I2C_READ);
		
		uint16_t value  = (i2c_read_byte(i2c) << 8);
		i2c_send_bit(i2c, I2C_ACK);
		
		value |= i2c_read_byte(i2c);
		i2c_send_bit(i2c, I2C_ACK);
		
		i2c_stop(i2c);
		
		return static_cast<float>(value&0x0FFF)*degScale;
	}
};