#pragma once

#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <stdint.h>

class AS5600
{
public:
	enum Status
	{
		MH			= 0x07, // Magnet too strong
		ML			= 0x01, // Magnet too weak
		MD			= 0x02  // Magnet OK
	};
private:	
	enum RegisterMap : uint8_t
	{
		ZMCO		= 0x00,
		ZPOS		= 0x02,
		MPOS		= 0x03,
		MANG		= 0x05,
		CONF 		= 0x07,
		RAW_ANGLE	= 0x0C,
		ANGLE		= 0x0E,
		STATUS		= 0x0B,
		AGC			= 0x1B,
		MAGNITUDE	= 0x1C,
		BURN		= 0xFF
	};
	
	static constexpr float degScale = 360.f / (1<<12);
	static constexpr uint8_t deviceId = 0x36;
	int32_t i2c = 0; 
	
public:
	AS5600()
	{
		i2c = wiringPiI2CSetup(deviceId);
	}
	
	const float GetAngleDegrees()
	{
		return static_cast<float>((wiringPiI2CReadReg8(i2c,RAW_ANGLE)<<8|wiringPiI2CReadReg8(i2c,RAW_ANGLE+1)) & 0x0FFF) * degScale;
	}
};