#pragma once
#include <wiringPi.h>

class Led
{
	//Settings
	uint8_t redPin = 0;
	uint8_t greenPin = 2;
	uint8_t bluePin = 3;	
public:
	Led(const uint8_t& redPin,const uint8_t& greenPin,const uint8_t& bluePin)
		:redPin(redPin),greenPin(greenPin),bluePin(bluePin)
	{
		pinMode(redPin,OUTPUT);
		pinMode(greenPin,OUTPUT);
		pinMode(bluePin,OUTPUT);
	}
	
	void SetColor(bool red,bool green, bool blue)
	{
		digitalWrite(redPin,red ? LOW : HIGH);
		digitalWrite(greenPin,green ? LOW : HIGH);
		digitalWrite(bluePin,blue ? LOW : HIGH);
	}
};