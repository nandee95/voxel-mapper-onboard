#pragma once
#include <SFML/Network.hpp>
#include <vector>
#include <thread>
#include <functional>
#include "Protocol.hpp"

class Monitor
{
	uint16_t port;
    bool active = true;
    bool connected = false;
    std::thread thread;
	sf::TcpListener listener;
    sf::TcpSocket socket;
	std::function <void(PacketControl)> onControl;
public:


    Monitor(uint16_t port) : port(port), thread(&Monitor::Thread,this)
    {
    }

    bool IsConnected()
    {
        return connected;
    }

    bool Send(void* data,size_t size)
    {
        return socket.send(data,size) == sf::Socket::Done;
    }

	void OnControl(std::function <void(PacketControl)> onControl)
	{
		this->onControl = onControl;
	}

    ~Monitor()
    {
		std::cout << "Closing connection..." << std::endl;
        active = false;
		socket.setBlocking(false);
		listener.setBlocking(false);
        socket.disconnect();
        if(thread.joinable()) thread.join();
		std::cout << "Connection closed!" << std::endl;
    }

private:
    static void Thread(Monitor* that)
    {
		that->listener.setBlocking(true);
        if(that->listener.listen(that->port)!= sf::Socket::Done)
        {
			std::cout << "Monitor failed to open port "<<that->port<<"!" << std::endl;
			exit(EXIT_FAILURE);
            return;
        }
        
        std::cout << "Monitor is listening on port "<<that->port<<"!" << std::endl;

        while(that->active)
        {
			if(that->listener.accept(that->socket) == sf::Socket::Done)
			{					
				that->connected = true;
				std::cout << "Monitor connected!" << std::endl;
				that->socket.setBlocking(true);
				size_t received;
				PacketType type;
				while(that->socket.receive(&type,sizeof(type),received) == sf::Socket::Done && that->active)
				{
					std::cout << "Packet received!" << std::endl;
					switch(type)
					{
						case PacketType::Packet_Control:
						{
							PacketControl packet;
							if(that->socket.receive(&packet,sizeof(packet),received) == sf::Socket::Done)
							{
								that->onControl(packet);
							}
						}
					}
					
					std::cout << "Packet processed!" << std::endl;
				}
				std::cout << "Socket exited!" << std::endl;
			}
			
			std::cout << "After if!" << std::endl;
        }
		
		std::cout << "Monitor thread exited!" << std::endl;
    }
};