#pragma once
#include <iostream>
#include <thread>
#include <chrono>
#include <cmath>
#include <chrono>

#include "Drive.hpp"
#include "LED.hpp"
#include "Protocol.hpp"
#include "Monitor.hpp"
#include "Kinect.hpp"
#include "ThreadPool.hpp"
#include "CompressionTask.hpp"

#include "PinOut.hpp"

class Application
{
	static constexpr uint16_t defaultPort = 3000;
	static constexpr uint8_t poolThreads = 3;
	Led led;
	Monitor monitor;
	Kinect kinect;
	bool running = true;
	ThreadPool pool;
	Drive drive;
public:
	Application(int32_t argc, char** argv) :
	monitor(argc == 2 ? std::atoi(argv[1]) : defaultPort),
	pool(poolThreads),
	led(PinOut::PIN_LED_RED,PinOut::PIN_LED_GREEN,PinOut::PIN_LED_BLUE)
	{	
		// Set color to yellow - initializing
		led.SetColor(1,1,0);
		
		// Init Kinect
		std::cout << "Initializing Kinect..." << std::endl;
		if(!kinect.Open())
		{
			FatalError("Failed to initialize the kinect!",4);
		}
		
		//Turn led to greeen
		led.SetColor(0,1,0);
		std::cout << "Program initialized!" << std::endl;
	}
	
	~Application()
	{
		led.SetColor(0,0,0);
	}
	
	int32_t Run()
	{
		kinect.OnData([&](void* data,size_t size){
			if(monitor.IsConnected())
			{
				char* copy = new char[size];
				memcpy(copy,data,size);
				pool.ClearTasks();
				pool.AddTask(std::make_shared<CompressionTask>(copy,size,&monitor,drive.GetPosition(),drive.GetOrientation()));
			}
		});
		
		monitor.OnControl([&](PacketControl packet){
			drive.Control(packet.leftright,packet.frontback);
		});
		
		// Main loop		
		while(running)
		{		
			kinect.Update();
			//drive.Update();
			//std::this_thread::sleep_for(std::chrono::milliseconds(30));
		}
		return EXIT_SUCCESS;
	}
	
protected:
	void FatalError(const std::string& message,const int16_t flashes=5)
	{
		std::cerr << "Fatal error: " <<  message << std::endl;
		for(int32_t i = 0;i < flashes; i++)
		{
			led.SetColor(1,0,0);
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
			led.SetColor(0,0,0);
			std::this_thread::sleep_for(std::chrono::milliseconds(500));
		}
		exit(EXIT_FAILURE);
	}
};