#pragma once
#define GLM_ENABLE_EXPERIMENTAL
#include "PID.hpp"
#include "Servo.hpp"
#include <thread>
#include <iomanip>
#include <glm/glm.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <thread>

#include "PID.hpp"
#include "PinOut.hpp"

#include "Servo.hpp"
#include "MPU9250.h"
#include "AS5600.hpp"
#include "AS5600_Software.hpp"

class Drive
{
	static constexpr float scale = 1000.f/850.f;
	static constexpr float wheelDiameter = 80;
	static constexpr float wheelPerimeter = wheelDiameter * 3.14159265;
	static constexpr float wheelDistance = 236/2;
	static constexpr float wheelPathLength = wheelDistance * 2 * 3.14159265;
	
	glm::vec3 position{0,0,0};
	MPU9250<AFS::A16G,GFS::G2000DPS, MFS::M16BITS> mpu;
	AS5600_Software leftRotary;
	AS5600 rightRotary;
	Servo leftServo;
	Servo rightServo;
	
	float lastLeftAngle=0,lastRightAngle=0;
	
	std::thread thread;
	bool running = true;
public:
	Drive() : 
		leftRotary(PinOut::SOFT_I2C_SCL,PinOut::SOFT_I2C_SDA),
		leftServo(PinOut::MOTOR_LEFT_FORWARD,PinOut::MOTOR_LEFT_BACKWARDS),
		rightServo(PinOut::MOTOR_RIGHT_FORWARD,PinOut::MOTOR_RIGHT_BACKWARDS),
		thread(&Thread,this)
	{
		lastLeftAngle = leftRotary.GetAngleDegrees();
		lastRightAngle = rightRotary.GetAngleDegrees();
	}
	
	~Drive()
	{
		running = false;
		if(thread.joinable()) thread.join();
	}
	
	float GetOrientation()
	{
		return mpu.getYaw();
	}
	
	glm::vec3 GetPosition()
	{
		return position;
	}
	
	void Control(const float& leftright, const float& forwardbackwards)
	{
		leftServo.Write(glm::clamp<float>(leftright + forwardbackwards,-1,1));
		rightServo.Write(glm::clamp<float>(-leftright + forwardbackwards,-1,1));
	}
	
	void Update()
	{
		mpu.update();
		float leftAngle = leftRotary.GetAngleDegrees() ;
		float rightAngle = 360 - rightRotary.GetAngleDegrees();
		
		float deltaLeftAngle = leftAngle - lastLeftAngle;
		float deltaRightAngle = rightAngle - lastRightAngle;
		
		if (deltaLeftAngle < -180) { deltaLeftAngle += 360; }
		else if (deltaLeftAngle > 180) { deltaLeftAngle -= 360; }
		if (deltaRightAngle < -180) { deltaRightAngle += 360; }
		else if (deltaRightAngle > 180) { deltaRightAngle -= 360;}
		
		const float leftArcLen = wheelDiameter * (deltaLeftAngle / 360.f); // Wheel travel
		const float rightArcLen = wheelDiameter * (deltaRightAngle / 360.f);
		
		const float leftArcAngle = leftArcLen / wheelPathLength * glm::two_pi<float>();
		const float rightArcAngle  = rightArcLen / wheelPathLength * glm::two_pi<float>();
		
		const glm::vec2 leftWheelPos(-wheelDistance / 2,leftArcLen);
		
		const glm::vec2 rightWheelPos(wheelDistance / 2,rightArcLen);
		
		const glm::vec3 deltaPos = glm::rotateZ(glm::vec3((leftWheelPos + rightWheelPos),0),-glm::radians(mpu.getYaw()) + glm::pi<float>());
		
		position += deltaPos * scale;
		
		lastLeftAngle = leftAngle;
		lastRightAngle = rightAngle;	
	}
	
	private:
	static void Thread(Drive* that)
	{
		that->mpu.setup();
		that->mpu.setGyroBias(0,-1.61743);
		that->mpu.setGyroBias(1,0.325928);
		that->mpu.setGyroBias(2,-0.674688);
		that->mpu.setAccBias(0,0.0167847);
		that->mpu.setAccBias(1,0);
		that->mpu.setAccBias(2,0);
		that->mpu.setMagBias(0,0);
		that->mpu.setMagBias(1,96.7809);
		that->mpu.setMagBias(2,278.178);
		that->mpu.setMagScale(0,1);
		that->mpu.setMagScale(1,1);
		that->mpu.setMagScale(2,1);
		while(that->running)
		{
			that->Update();
			//std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
	}
};