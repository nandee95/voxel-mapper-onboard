#pragma once


#include <chrono>

class PID
{
	float kP, kI, kD;
	float P=0, I=0, D=0;

	float targetValue=0,lastSample=0;

	std::chrono::high_resolution_clock::time_point last = std::chrono::high_resolution_clock::now();
public:
	PID(const float& kP, const float& kI, const float& kD) : kP(kP),kI(kI),kD(kD) {
	}

	void SetTarget(const float& target) {
		targetValue = target;
	}

	float Process(const float& sample) {
		const auto now = std::chrono::high_resolution_clock::now();
		const float deltaTime = std::chrono::duration_cast<std::chrono::microseconds>(now - last).count() / 1000000.0;
		last = now;
		const float error = targetValue - sample;
		P = error * kP;
		I += (error * kI) * deltaTime;
		D = (lastSample - sample) * kD / deltaTime;
		lastSample = sample;
		return P + I + D;
	}
};
