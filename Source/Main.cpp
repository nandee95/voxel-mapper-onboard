#include "Application.hpp"

#include <signal.h>
#include <memory>

std::shared_ptr<Application> app;

void sighandler(int) {
    std::cout << "Closing application..."<<std::endl;
	app.reset();
    std::cout << "Exiting..."<<std::endl;
	exit(EXIT_SUCCESS);
}

int32_t main(int32_t argc, char** argv)
{
	if (signal(SIGINT, &sighandler) == SIG_ERR) {
        std::cerr << "Could not set signal handler"<<std::endl;
        return EXIT_FAILURE;
    }
	
	std::cout << "Initializing WiringPi..." << std::endl;
	wiringPiSetup();
	
	app = std::make_shared<Application>(argc,argv);
	return app->Run();
}