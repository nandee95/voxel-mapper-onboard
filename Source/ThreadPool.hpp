#pragma once
#include <thread>
#include <mutex>
#include <queue>
#include <vector>
#include <condition_variable>
#include <iostream>
#include <chrono>
#include <algorithm>
#include "Task.hpp"

class ThreadPool
{
	std::queue<std::shared_ptr<Task>> tasks;
	bool active = true;
	std::mutex mu;
	std::condition_variable cv;
	std::vector<std::thread> threads;
public:
	ThreadPool(const int32_t& size)
	{
		for (int32_t i = 0; i < size; i++)
		{
			threads.push_back(std::thread(Thread,this,i));
		}
	}

	~ThreadPool()
	{
		std::cout << "Closing threadpool ..." << std::endl;
		active = false;
		cv.notify_all();
		for (auto& t : threads)
		{
			if (t.joinable()) t.join();
		}
		std::cout << "Threadpool closed!" << std::endl;
	}

	void ClearTasks()
	{
		std::lock_guard<std::mutex> gu(mu);
		tasks = std::queue<std::shared_ptr<Task>>();
	}
	void AddTask(std::shared_ptr<Task> task)
	{
		std::lock_guard<std::mutex> gu(mu);
		tasks.push(task);
		cv.notify_one();
	}
private:
	static void Thread(ThreadPool* pool,int32_t id)
	{
		while (pool->active)
		{
			std::shared_ptr<Task> task;
			{
				std::unique_lock<std::mutex> lock(pool->mu);
				pool->cv.wait(lock);
				if (!pool->tasks.empty())
				{
					task = pool->tasks.back();
					pool->tasks.pop();
				}
				else continue;
			}
			
			task->Preform();
		}
	}
};
