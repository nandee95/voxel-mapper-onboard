#pragma once
#include <libfreenect.h>
#include <thread>
#include <cstring>
#include <functional>
#include <glm/vec2.hpp>
//#include <turbojpeg.h>

class Kinect
{
	//static constexpr int JpegQuality = 100; // 0 - 100
	//tjhandle tjCompressor;
	const glm::ivec2 resolution {640,480};
	freenect_context* fn_ctx;
	freenect_device* fn_dev;
	uint8_t* data;
	
	bool open=false;
	
	std::function<void(void*,size_t)> onData;
public:
	Kinect() : data(new uint8_t[GetSize()])
	{
		//tjCompressor = tjInitCompress();
	}
	
	~Kinect()
	{
		if(open)
		{
			std::cout << "Stopping kinect..." << std::endl;
			delete[] data;
			freenect_stop_depth(fn_dev);
			freenect_close_device(fn_dev);
			//freenect_shutdown(fn_ctx);
			std::cout << "Kinect stopped!" << std::endl;
		}
	}
	
	void OnData(std::function<void(void*,size_t)> e)
	{
		onData = e;
	}
	
	uint8_t* GetData()
	{
		return data;
	}
	
	const size_t GetSize() const
	{
		return resolution.x*resolution.y*2;
	}
	
	const glm::ivec2 GetResolution() const
	{
		return resolution;
	}
	
	bool Open()
	{
		int ret = freenect_init(&fn_ctx, NULL);
		if (ret < 0)
			return false;
		
		ret = freenect_open_device(fn_ctx, &fn_dev, 0);
		if (ret < 0)
		{
			freenect_shutdown(fn_ctx);
			return false;
		}
		
		
		//freenect_set_log_level(fn_ctx, FREENECT_LOG_DEBUG);
		freenect_select_subdevices(fn_ctx, FREENECT_DEVICE_CAMERA);
		freenect_set_flag(fn_dev, FREENECT_NEAR_MODE, FREENECT_ON);
		ret = freenect_set_depth_mode(fn_dev, freenect_find_depth_mode(FREENECT_RESOLUTION_MEDIUM, FREENECT_DEPTH_MM));
		if (ret < 0)
		{
			freenect_shutdown(fn_ctx);
			return false;
		}
	
		freenect_set_depth_callback(fn_dev, Kinect::depth_cb);
		freenect_set_user(fn_dev,this);
	
		ret = freenect_start_depth(fn_dev);
		if (ret < 0)
		{
			freenect_shutdown(fn_ctx);
			return false;
		}
		open = true;
		return true;
	}
	
	void Update()
	{
		
		freenect_process_events(fn_ctx);
	}
	
private:
	
	static void depth_cb(freenect_device* dev, void* data, uint32_t timestamp)
	{
		Kinect* that = reinterpret_cast<Kinect*>(freenect_get_user(dev));
		that->onData(data,that->GetSize());		
	}
	
};