#pragma once

#include "Task.hpp"
#include <zlib.h>

class CompressionTask : public Task
{
	char* data=nullptr;
	size_t size;
	
	static std::mutex mu;
	static PacketType type;
	
	PacketFullUpdate packet;
	Monitor* monitor;
public:
	CompressionTask(char* data,size_t size,Monitor* monitor,const glm::vec3& position,const float& angle) : data(data) ,size(size),monitor(monitor)
	{
		packet.position = position;
		packet.angle = angle;
		packet.resolution = {640,480};
		packet.totalSize = 640*480*2;
	}
	
	~CompressionTask()
	{
		delete[] data;
	}
	
	virtual void Preform() override
	{
		uLongf outSize;
		std::vector<char> outData;
		outData.resize(640*480*2);
		auto ret = compress2(reinterpret_cast<Bytef*>(outData.data()), &outSize, reinterpret_cast<Bytef*>(data), size,1);
		if(ret != Z_OK)
		{
			std::cerr << "Failed to compress frame ERRNO: "<< ret <<"!"<<std::endl;
			return;
		}

		{
			std::lock_guard<std::mutex> gu(mu);
			packet.compressedSize = outSize;
			monitor->Send(&type,sizeof(type));
			monitor->Send(&packet,sizeof(packet));
			monitor->Send(outData.data(),outSize);
		}
	}
};

std::mutex CompressionTask::mu;
PacketType CompressionTask::type = PacketType::Packet_FullUpdate;