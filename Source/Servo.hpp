#pragma once
#include <wiringPi.h>
#include <softPwm.h>

class Servo
{
	uint8_t forwardPin,backwardsPin;
public:
	Servo(const uint8_t& forwardPin,const uint8_t& backwardsPin) : forwardPin(forwardPin), backwardsPin(backwardsPin)
	{
		pinMode(forwardPin,OUTPUT);
		pinMode(backwardsPin,OUTPUT);
		
		softPwmCreate(forwardPin,0, 100);
		softPwmCreate(backwardsPin,0, 100);
	}
	
	bool Write(const float& value)
	{
		if(value < -1.f || value > 1.f) return false;
		
		if(value > 0)
		{
			softPwmWrite(backwardsPin,0);
			softPwmWrite(forwardPin,static_cast<int>(value * 100.f));
		}
		else if(value < 0)
		{
			softPwmWrite(forwardPin,0);
			softPwmWrite(backwardsPin,static_cast<int>(std::fabs(value * 100.f)));
		}
		else
		{
			softPwmWrite(backwardsPin,0);
			softPwmWrite(forwardPin,0);
		}
		return true;		
	}
};